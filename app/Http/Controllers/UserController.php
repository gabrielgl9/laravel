<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;

class UserController extends Controller
{
    //

    public function index()
    {
       return view("user.authenticate");
    }

    public function authenticate(Request $request)
    {
      $user = Models\User::where('email', $request->input('email'))->where('password', $request->input('password'))->first();
      $user = json_decode($user);
      if (isset($user)){
          session(['user' => $user]);
          return redirect()->action('ProductController@feed');
      }
      return redirect()->back()->withInput();

      //return redirect()->route('user.index');
    }

    public function logout()
    {
        session_unset();
        return redirect()->route('user.index');
    }

    public function create()
    {
      return view("user.create");
    }

    public function store(Request $request)
    {
          Models\User::create($request->all());
          return back();
    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $user = json_decode(Models\User::findOrFail($id));
        return view("user.perfil", ["user" => $user]);
    }

    public function update($id)
    {

    }

    public function delete($id)
    {

    }
}
