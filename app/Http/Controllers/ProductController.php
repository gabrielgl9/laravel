<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;


class ProductController extends Controller
{
    //
    public function index()
    {
        $products = Models\Product::where('user_id', session('user')->id)->paginate(3);
        return view('product.index', compact('products', 'products'));
    }

    public function feed()
    {
        $products = Models\Product::paginate(3);
        return view('panel.main', compact('products'));
    }

    public function filter(Request $request)
    {
        $dataForm = $request->all();

        $products = Models\Product::where('user_id', session('user')->id)
                    ->where(function ($query) use ($request){
                            if ($request->has('id'))
                                    $query->where('id', $request['id']);
                            if ($request->has('title'))
                                    $query->where('title', $request['title']);
                            if ($request->has('categories'))
                                    $query->where('categories', $request['categories']);
                            if ($request->has('value'))
                                    $query->where('value', $request['value']);
                    })->paginate(3);

        return view('product.index', compact('products', 'dataForm'));

    }

    public function create()
    {
        return view('product.create');
    }

    public function store(Request $request)
    {
        Models\Product::create($request->all());
        return redirect()->back();
    }

    public function show($id)
    {
        $product = json_decode(Models\Product::findOrFail($id));
        return view('product.show', ['product'=>$product]);
    }

    public function edit($id)
    {
        $product = json_decode(Models\Product::findOrFail($id));
        return view('product.edit', ['product' => $product]);
    }

    public function update(Request $request, $id)
    {
        $product = Models\Product::findOrFail($id);
        $product->update($request->all());
        return redirect()->route('product.index');
    }

    public function destroy($id)
    {
        $product = Models\Product::find($id);
        $product->delete();
        return redirect()->back();
    }
}
