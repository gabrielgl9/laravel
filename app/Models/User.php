<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    //
    protected $table = "users";

    public $name;
    public $telephone;
    public $email;
    public $password;
    public $created_at;
    public $updated_at;

    protected $fillable = ['name', 'telephone', 'email', 'password'];

    //protected $guarded = ['remember_token'];
}
