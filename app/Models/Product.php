<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //

    protected $table = "products";

    public $id;
    public $title;
    public $description;
    public $categories;
    public $value;
    public $quantity;
    public $weight;
    public $user_id;

    protected $fillable = ["id", "title", "description", "categories", "value", "quantity", "weight", "user_id"];
}
