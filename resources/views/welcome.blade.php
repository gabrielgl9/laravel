@extends('template')

@section('title')
Bem vindo a Loja Viegod
@endsection('content')

@section('content')
<ul class="nav justify-content-end">
  <li class="nav-item">
    <a class="nav-link" href="{{route('user.index')}}">Login</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{route('user.create')}}">Cadastro</a>
  </li>
</ul>

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" href="#">
      <img src="http://pluspng.com/img-png/bootstrap-logo-png-logo-228.png" width="30" height="30" class="d-inline-block align-top" alt="">
      Bootstrap
    </a>
  <ul class="navbar-nav" id="right-text">
      <li class="nav-item">
        <a class="nav-link" href="#">Sobre</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Principios</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Equipe</a>
      </li>
    </ul>
</nav>

<img src="https://i.pinimg.com/originals/3f/73/d5/3f73d57e3252bb60f1b622e7f6595a6e.jpg" class="img-fluid" style="width: 100%;" alt="Responsive image">

<br><br><br><p class="h1 text-center" >Sobre Nós</p><br><br><br><br>


<div class="container">

  <div class="row">
      <div class="col-md-4 offset-md-1">
        <i class="fas fa-address-card" style="font-size: 130px; color: #6dced5"></i>
      </div>
      <div class="col-md-4">
        <i class="fas fa-building" style="font-size: 130px; color: #6dced5"></i>
      </div>
      <div class="col-md-3">
        <i class="fas fa-user-friends" style="font-size: 130px; color: #6dced5;"></i>
      </div>
      <div class="w-100"></div>
      <div class="col-md-4 lead blockquote"><br><br>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </div>
      <div class="col-md-4 lead blockquote"><br><br>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </div>
      <div class="col-md-4 lead blockquote"><br><br>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </div>
  </div>
</div>
<br><br><br>

<div style="background: #31428e; height: 500px; width:100%; color: white;">
  <p class="h1 text-center" >Princípios</p><br><br>

  <div class="container">
    <div class="row">
      <div class="col-md-4 offset-md-1">
        <i class="fas fa-dog" style="font-size: 130px; color: #6dced5"></i>
      </div>
      <div class="col-md-4">
        <i class="fas fa-people-carry" style="font-size: 130px; color: #6dced5"></i>
      </div>
      <div class="col-md-3">
        <i class="fab fa-angellist" style="font-size: 130px; color: #6dced5;"></i>
      </div>
      <div class="w-100"></div>
      <div class="col-md-4 lead blockquote"><br><br>
        Lorem ipsum dolor sit amet, non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </div>
      <div class="col-md-4 lead blockquote"><br><br>
        Lorem ipsum dolor sit amet, non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </div>
      <div class="col-md-4 lead blockquote"><br><br>
        Lorem ipsum dolor sit amet, non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </div>
    </div>
  </div>
</div>

<br><p class="h1 text-center" >Equipe</p><br><br><br><br>

<div class="container">
  <div class="row">
    <div class="col-md-4 offset-md-1">
      <img src="https://scontent.fpoa7-2.fna.fbcdn.net/v/t1.0-9/24176853_1461725863941450_8501037210785552697_n.jpg?_nc_cat=103&_nc_ht=scontent.fpoa7-2.fna&oh=fd17c958261cae2f0d3a938dbcf457a7&oe=5D7125C4" class="rounded float-left" alt="Gabriel Viegas" style="width: 300px; height:300px;">
    </div>
    <div class="col-md-6 lead blockquote">
      Lorem ipsum dolor sit amet, non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      Lorem ipsum dolor sit amet, non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      Lorem ipsum dolor sit amet, non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      Lorem ipsum dolor sit amet, non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </div>
  </div><br><br>
  <div class="row">
    <div class="col-md-4 offset-md-1">
      <img src="https://scontent.fpoa7-2.fna.fbcdn.net/v/t1.0-9/56520148_2325978961058108_7497734536160083968_n.jpg?_nc_cat=104&_nc_ht=scontent.fpoa7-2.fna&oh=31abba50f78dd70ec11d4ab1bc1573e7&oe=5D2A8154" class="rounded float-left" alt="Gabriel Viegas" style="width: 300px; height:300px;">
    </div>
    <div class="col-md-6 lead blockquote">
      Lorem ipsum dolor sit amet, non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      Lorem ipsum dolor sit amet, non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      Lorem ipsum dolor sit amet, non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      Lorem ipsum dolor sit amet, non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </div>
  </div>
</div>

<br><br><br><br>

@endsection('content')
