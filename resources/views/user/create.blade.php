@extends('template')

@section('title')
Cadastrar Usuário
@endsection('content')

@section('content')
<div class="container-fluid">
  <div class="row no-gutter">
    <div class="col-md-8 col-lg-8 offset-md-2">
      <div class="login d-flex align-items-center py-5">
        <div class="container">
          <div class="row">
            <div class="col-md-9 col-lg-8 mx-auto">
              <h3 class="login-heading mb-4">Cadastre-se agora</h3>
              <form action="{{route('user.store')}}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-label-group">
                  <input type="text" id="nome" name="name" class="form-control" placeholder="Nome completo" value="{{ old('name') }}" required autofocus>
                  <label for="nome"></label>
                </div>

                <div class="form-label-group">
                  <input type="tel" id="telefone" name="telephone" class="form-control" placeholder="Telefone" value="{{ old('telephone') }}" required autofocus>
                  <label for="telefone"></label>
                </div>

                <div class="form-label-group">
                  <input type="email" id="email" name="email" class="form-control" placeholder="Endereço de Email" value="{{ old('email') }}" required autofocus>
                  <label for="email"></label>
                </div>

                <div class="form-label-group">
                  <input type="password" id="senha" name="password" class="form-control" placeholder="Senha" value="{{ old('password') }}" required>
                  <label for="senha"></label>
                </div>

                <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Cadastrar</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection('content')
