@extends('navbar')

@section('lis')
<li class="nav-item">
    <a class="nav-link" href="/home">Home</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{route('product.create')}}">Cadastrar Produto</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{route('product.index')}}">Listar seus produtos</a>
</li>
<li class="nav-item active dropdown">
    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown"> {{session('user')->name}}</a>
    <div class="dropdown-menu">
        <a href="{{ route('user.edit', ['id' => session('user')->id]) }}" class="dropdown-item">Meu Perfil</a>
        <a href="/logout" class="dropdown-item">Sair</a>
    </div>
</li>
@endsection('lis')

@section('h1', 'Meu Perfil')
@section('text', 'Dados pertencentes a sua conta. Você poderá editá-los a vontade!')
@section('content2')

<div class="row">
    <div class="col-md-9 col-lg-8">
        <form action="{{ route('user.update', ['id' => $user->id]) }}" method="POST">
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-label-group">
                <input type="text" id="name" name="name" class="form-control" placeholder="Seu nome" value="{{ $user->name  }}" required autofocus>
                <label for="name"></label>
            </div>

            <div class="form-label-group">
                <input type="text" id="telephone" name="telephone" class="form-control" placeholder="Telefone" value="{{ $user->telephone }}" required autofocus>
                <label for="telephone"></label>
            </div>

            <div class="form-label-group">
                <input type="email" id="email" name="email" class="form-control" placeholder="E-mail" value="{{ $user->email }}" required autofocus>
                <label for="email"></label>
            </div>

            <button class="btn btn-lg btn-primary btn-block text-uppercase font-weight-bold mb-2" style="background: green; border-color: green" type="submit">Atualizar Dados</button>
        </form>
    </div>
</div>

@endsection('content2')
