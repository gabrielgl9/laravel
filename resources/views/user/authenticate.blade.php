@extends('template')

@section('title')
login
@endsection('content')

@section('content')
<div class="container-fluid">
  <div class="row no-gutter">
    <div class="col-md-8 col-lg-8 offset-md-2">
      <div class="login d-flex align-items-center py-5">
        <div class="container">
          <div class="row">
            <div class="col-md-9 col-lg-8 mx-auto">
              <h3 class="login-heading mb-4">Seja bem vindo de volta</h3>
              <form method="post" action="/user/authenticate">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-label-group">
                  <input type="email" name="email" value="{{ old('email') }}" id="inputEmail" class="form-control" placeholder="Endereço de Email" required autofocus>
                  <label for="inputEmail">Email</label>
                </div>

                <div class="form-label-group">
                  <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Senha" required>
                  <label for="inputPassword">Senha</label>
                </div>

                <div class="custom-control custom-checkbox mb-3">
                  <input type="checkbox" class="custom-control-input" id="customCheck1">
                  <label class="custom-control-label" for="customCheck1">Lembrar minha senha</label>
                </div>
                <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Sign in</button>
                <div class="text-center">
                  <a class="small" href="#">Esqueci minha senha!</a></div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection('content')
