@extends('template')
@section('title', 'Adminx')

@section('content')
<header>
    <div class="row">
        <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
            <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Loja Viegod</a>
            <ul class="navbar-nav px-3">
                <li class="nav-item text-nowrap">
                    <a class="nav-link" href="#">Sair</a>
                </li>
            </ul>
        </nav>
    </div>
</header>

<section>
    <div class="container-fluid">
        <div class="row" style="height: 100%;">
            <div class="col-2" style="background: #3c4167;">
                <ul class="list-unstyled mt-5">

                    <li class="pt-3"> <a href="" style="color: white"> <i class="fas fa-home pr-3" style="font-size: 30px;"></i> Dashboard </a> </li>
                    <li class="pt-3"> <a href="" style="color: white"> <i class="fas fa-cart-plus pr-3" style="font-size: 30px;"></i> Produtos </a> </li>
                    <li class="pt-3"> <a href="" style="color: white"> <i class="fas fa-layer-group pr-3" style="font-size: 30px;"></i> Categorias </a> </li>
                    <li class="pt-3"> <a href="" style="color: white"> <i class="fas fa-users pr-3" style="font-size: 30px;"></i> Usuários </a> </li>
                </ul>
            </div>
            <div class="col">
            </div>
        </div>
    </div>
</section>

<!--
<div class="container-fluid">
<div class="row mt-4">
<div class="col-sm-4">
<div class="sidebar">
<ul class="list-group mt-1">
<li class="list-group-item"> <a href=""> Dashboard </a> </li>
<li class="list-group-item"> <a href=""> Produtos </a> </li>
<li class="list-group-item"> <a href=""> Categorias </a> </li>
<li class="list-group-item"> <a href=""> Usuários </a> </li>
</ul>
</div>
</div>
</div>
</div>

-->
@endsection
