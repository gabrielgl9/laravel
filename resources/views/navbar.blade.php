<!-- Navigation -->
@extends('template')
@section('title', 'Loja Viegod')
@section('content')
<nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
    <div class="container">
        <a class="navbar-brand" href="#">
          <img src="http://placehold.it/150x50?text=Logo" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                @yield('lis')
            </ul>
        </div>
    </div>
</nav>
<!-- Page Content -->
<div class="container">
    <h1 class="mt-4">@yield('h1')</h1>
    <p>@yield('text')</p>
    @yield('content2')
@endsection('content')
