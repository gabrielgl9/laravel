@extends('navbar')
@section('lis')
<li class="nav-item active">
    <a class="nav-link" href="/home">Home</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{route('product.create')}}">Cadastrar Produto</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{route('product.index')}}">Listar seus produtos</a>
</li>

<li class="nav-item dropdown">
    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown"> {{session('user')->name}}</a>
    <div class="dropdown-menu">
        <a href="{{ route('user.edit', ['id' => session('user')->id]) }}" class="dropdown-item">Meu Perfil</a>
        <a href="/logout" class="dropdown-item">Sair</a>
    </div>
</li>
@endsection('lis')
@section('h1', 'Produtos da Loja')
@section('text', 'Estes são todos os produtos disponíveis na Loja Virtual Viegod. Você também poderá comprá-los caso desejar.')
@section('content2')

<div class="row">
    @foreach ($products as $product)
    <div class="col-lg-4 col-sm-6 mb-4">
        <div class="card h-100">
            <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
            <div class="card-body">
                <h4 class="card-title">
                    <a href="#">{{json_decode($product)->title}}</a>
                </h4>
                <p class="card-text">
                    {{json_decode($product)->description}}
                </p>
            </div>
        </div>
    </div>
    @endforeach
</div>

<!-- Pagination -->
<div class="container pagination justify-content-center">
    @if (isset($dataForm))
        {!! $products->appends($dataForm)->links("pagination::bootstrap-4") !!}
    @else
        {!! $products->links("pagination::bootstrap-4") !!}
    @endif
</div>

</div>
@endsection('content2')
<!-- /.container -->
