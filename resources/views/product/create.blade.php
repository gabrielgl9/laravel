@extends('navbar')

@section('lis')
<li class="nav-item">
    <a class="nav-link" href="/home">Home</a>
</li>
<li class="nav-item active">
    <a class="nav-link" href="{{route('product.create')}}">Cadastrar Produto</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{route('product.index')}}">Listar seus produtos</a>
</li>
<li class="nav-item dropdown">
    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown"> {{session('user')->name}}</a>
    <div class="dropdown-menu">
        <a href="{{ route('user.edit', ['id' => session('user')->id]) }}" class="dropdown-item">Meu Perfil</a>
        <a href="/logout" class="dropdown-item">Sair</a>
    </div>
</li>
@endsection('lis')

@section('h1', 'Cadastrar Produto')
@section('text', 'Insira aqui os dados do produto e ele ficará visível a todos usuário, que poderão optar por comprá-lo.')
@section('content2')

<div class="row">
    <div class="col-md-9 col-lg-8">
        <form action="{{route('product.store')}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="user_id" value="{{session('user')->id}}">
            <div class="form-label-group">
                <input type="text" id="title" name="title" class="form-control" placeholder="Informe o título do produto" value="{{ old('title') }}" required autofocus>
                <label for="title"></label>
            </div>

            <div class="form-label-group">
                <input type="text" id="description" name="description" class="form-control" placeholder="Descrição" value="{{ old('description') }}" required autofocus>
                <label for="description"></label>
            </div>

            <div class="form-label-group">
                <input type="text" id="categories" name="categories" class="form-control" placeholder="Categoria" value="{{ old('categories') }}" required autofocus>
                <label for="categories"></label>
            </div>

            <div class="form-label-group">
                <input type="number" id="value" name="value" class="form-control" placeholder="Preço" value="{{ old('value') }}" required>
                <label for="value"></label>
            </div>

            <div class="form-label-group">
                <input type="number" id="quantity" name="quantity" class="form-control" placeholder="Quantidade" value="{{ old('quantity') }}" required>
                <label for="quantity"></label>
            </div>

            <div class="form-label-group">
                <input type="number" id="weight" name="weight" class="form-control" placeholder="Peso" value="{{ old('weight') }}" required>
                <label for="weight"></label>
            </div>

            <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Cadastrar Produto</button>
        </form>
    </div>
</div>
@endsection('content2')
