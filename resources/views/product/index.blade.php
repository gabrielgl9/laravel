@extends('navbar')

@section('lis')
<li class="nav-item">
    <a class="nav-link" href="/home">Home</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{route('product.create')}}">Cadastrar Produto</a>
</li>
<li class="nav-item active">
    <a class="nav-link" href="{{route('product.index')}}">Listar seus produtos</a>
</li>
<li class="nav-item dropdown">
    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown"> {{session('user')->name}}</a>
    <div class="dropdown-menu">
        <a href="{{ route('user.edit', ['id' => session('user')->id]) }}" class="dropdown-item">Meu Perfil</a>
        <a href="/logout" class="dropdown-item">Sair</a>
    </div>
</li>
@endsection('lis')

@section('h1', 'Listagem dos seus produtos')
@section('text', 'Estes são todos os produtos que você cadastrou no sistema. Você poderá editar, excluir e visualizar eles.')
@section('content2')

<div class="row">
    <form method="GET" action="/product/filter">
        <div class="form-row">
            <div class="col-sm-2">
                <input type="number" name="id" class="form-control" placeholder="ID">
            </div>
            <div class="col-sm-3">
                <input type="text" name="title" class="form-control" placeholder="Título">
            </div>
            <div class="col-sm-3">
                <input type="text" name="categories" class="form-control" placeholder="Categoria">
            </div>
            <div class="col-sm-2">
                <input type="text" name="value" class="form-control" placeholder="Preço">
            </div>
            <div class="col-sm-2">
                <button type="submit" class="btn btn-primary">Filtrar Produto</button>
            </div>
        </div>
    </form>
</div>

<div class="row">
    <table class="table table-bordered">
        <thead class="thead-light">
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Título</th>
                <th scope="col">Categoria</th>
                <th scope="col">Preço</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>

            @forelse($products as $key => $product)
            <tr>
                <th scope="row">{{json_decode($product)->id}}</th>
                <td>{{json_decode($product)->title}}</td>
                <td>{{json_decode($product)->categories}}</td>
                <td>{{json_decode($product)->value}}</td>
                <td>
                    <a href="{{ route('product.show', ['id' => json_decode($product)->id]) }}" class="btn btn-primary btn-xs center-align" style="background: #275ba0; border-color: blue;">
                        <i class="fas fa-eye"></i>
                    </a>
                    <a href="{{ route('product.edit', ['id' => json_decode($product)->id]) }}" class="btn btn-primary btn-xs center-align" style="background: #275421; border-color: green;">
                        <i class="fas fa-edit"></i>
                    </a>
                    <button class="btn btn-primary btn-xs center-align" onclick="clickModal({{json_decode($product)->id}})" data-target="myModal" data-toggle="modal" style="background: #ac111e; border-color: red;">
                        <i class="fas fa-trash-alt"></i>
                    </button>
                </td>
            </tr>

            <!-- MODAL DE DELETE -->
            <div class="modal fade" id="myModal{{json_decode($product)->id}}">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Deletando o item: {{$product->title}}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            O item selecionado está para ser removido. Você tem certeza que deseja fazer isso?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                            <form method="post" action="{{ route('product.destroy', ['id' => json_decode($product)->id]) }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="_method" value="DELETE">
                                <button type="submit" class="btn btn-primary">Confirmar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            @empty
            @endforelse
        </tbody>
    </table>
</div>

<div class="container">
    @if (isset($dataForm))
        {!! $products->appends($dataForm)->links("pagination::bootstrap-4") !!}
    @else
        {!! $products->links("pagination::bootstrap-4") !!}
    @endif
</div>

@endsection('content2')
