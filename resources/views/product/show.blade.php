@extends('navbar')

@section('lis')
<li class="nav-item">
    <a class="nav-link" href="/home">Home</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{route('product.create')}}">Cadastrar Produto</a>
</li>
<li class="nav-item active">
    <a class="nav-link" href="{{route('product.index')}}">Listar seus produtos</a>
</li>
<li class="nav-item dropdown">
    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown"> {{session('user')->name}}</a>
    <div class="dropdown-menu">
        <a href="{{ route('user.edit', ['id' => session('user')->id]) }}" class="dropdown-item">Meu Perfil</a>
        <a href="/logout" class="dropdown-item">Sair</a>
    </div>
</li>
@endsection('lis')

@section('h1', 'Visualizar Produto')
@section('text', 'Abaixo encontram-se os dados referentes ao produto selecionado.')
@section('content2')

<div class="row">
    <div class="col-lg-4 col-sm-6 mb-4">
        <div class="card h-100">
            <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
            <div class="card-body">
                <h4 class="card-title">
                    <a href="#">{{$product->title}}</a>
                </h4>
                <p class="card-text">{{$product->description}}</p>
                <hr>
                Categoria: {{$product->categories}}</br>
                Preço: {{$product->value}}<br>
                Quantidade: {{$product->quantity}}<br>
                Peso: {{$product->weight}}<br>
            </div>
        </div>
    </div>
</div>


@endsection('content2')
