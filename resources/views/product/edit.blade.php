@extends('navbar')

@section('lis')
<li class="nav-item">
    <a class="nav-link" href="/home">Home</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{route('product.create')}}">Cadastrar Produto</a>
</li>
<li class="nav-item active">
    <a class="nav-link" href="{{route('product.index')}}">Listar seus produtos</a>
</li>
<li class="nav-item dropdown">
    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown"> {{session('user')->name}}</a>
    <div class="dropdown-menu">
        <a href="{{ route('user.edit', ['id' => session('user')->id]) }}" class="dropdown-item">Meu Perfil</a>
        <a href="/logout" class="dropdown-item">Sair</a>
    </div>
</li>
@endsection('lis')

@section('h1', 'Editar Produto')
@section('text', 'Abaixo encontram-se os dados referentes ao produto selecionado. Cuidado ao confirmar suas mudanças, elas serão salvas.')
@section('content2')

<div class="row">
    <div class="col-md-9 col-lg-8">
        <form action="{{ route('product.update', ['id' => $product->id]) }}" method="POST">
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-label-group">
                <input type="text" id="title" name="title" class="form-control" placeholder="Informe o título do produto" value="{{ $product->title  }}" required autofocus>
                <label for="title"></label>
            </div>

            <div class="form-label-group">
                <input type="text" id="description" name="description" class="form-control" placeholder="Descrição" value="{{ $product->description }}" required autofocus>
                <label for="description"></label>
            </div>

            <div class="form-label-group">
                <input type="text" id="categories" name="categories" class="form-control" placeholder="Categoria" value="{{ $product->categories }}" required autofocus>
                <label for="categories"></label>
            </div>

            <div class="form-label-group">
                <input type="number" id="value" name="value" class="form-control" placeholder="Preço" value="{{ $product->value }}" required>
                <label for="value"></label>
            </div>

            <div class="form-label-group">
                <input type="number" id="quantity" name="quantity" class="form-control" placeholder="Quantidade" value="{{ $product->quantity }}" required>
                <label for="quantity"></label>
            </div>

            <div class="form-label-group">
                <input type="number" id="weight" name="weight" class="form-control" placeholder="Peso" value="{{ $product->weight }}" required>
                <label for="weight"></label>
            </div>

            <button class="btn btn-lg btn-primary btn-block text-uppercase font-weight-bold mb-2" style="background: green; border-color: green" type="submit">Editar Produto</button>
        </form>
    </div>
</div>

@endsection('content2')
